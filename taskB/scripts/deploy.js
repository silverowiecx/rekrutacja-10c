require("@nomiclabs/hardhat-ethers");

async function main() {
    // Grab the contract factory 
    const Shape = await ethers.getContractFactory("Shape");
 
    // Start deployment, returning a promise that resolves to a contract object
    const shape = await Shape.deploy(); // Instance of the contract 
    console.log("Contract deployed to address:", shape.address);

    const FractionalisedShape = await ethers.getContractFactory("FractionalisedShape");
    const fractionalisedShape = await FractionalisedShape.deploy("", shape.address);
    console.log("contract fractionalised deployed to addr:", fractionalisedShape.address);
 }
 
 main()
   .then(() => process.exit(0))
   .catch(error => {
     console.error(error);
     process.exit(1);
   });