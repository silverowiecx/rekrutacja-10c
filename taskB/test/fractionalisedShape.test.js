const { expect } = require("chai");
const assert = require('assert');


let shape;
let fractionalisedShape;
let accounts;

beforeEach (async () => {
    accounts = await ethers.getSigners(3);
    shape = await((await ethers.getContractFactory("Shape")).deploy());
    await shape.deployed();
    fractionalisedShape = await((await ethers.getContractFactory("FractionalisedShape")).deploy(shape.address));
    await fractionalisedShape.deployed();
    await shape.setFractionalisedAddress(fractionalisedShape.address);
});



describe("Shape", async function () {
  it("deploys and mints", async function () {
    assert((await shape.balanceOf(accounts[0].address)) == 0x0a);
  });

  it("transfers", async function (){
    await shape.connect(accounts[0]).transferFrom(accounts[0].address, accounts[1].address, 2);
    assert((await shape.balanceOf(accounts[1].address)) == 0x01);
  });
});

describe("FractionalisedShape", async function() {
    it("mints", async function() {
        await shape.connect(accounts[0]).approve(fractionalisedShape.address, 5);
        await fractionalisedShape.connect(accounts[0]).mint(5, 1000000);
        assert((await fractionalisedShape.balanceOf(accounts[0].address, 5)) == 0xF4240);
  
        await fractionalisedShape.connect(accounts[0]).safeTransferFrom(accounts[0].address, accounts[1].address, 5, 100, []);
        assert((await(fractionalisedShape.balanceOf(accounts[0].address, 5)) == 999900));

        await shape.connect(accounts[0]).transferFrom(accounts[0].address, accounts[2].address, 5);

        await fractionalisedShape.connect(accounts[0]).safeTransferFrom(accounts[0].address, accounts[1].address, 5, 999900, []);
        assert((await fractionalisedShape.balanceOf(accounts[0].address, 5)) == 0);
        assert((await shape.ownerOf(5)) == accounts[1].address);

        // await fractionalisedShape.connect(accounts[1]).safeTransferFrom(accounts[0].address, accounts[0].address, 5, 999900, []);
        // console.log(await fractionalisedShape.);
        await fractionalisedShape.connect(accounts[1]).redeem(5);
        // assert((await fractionalisedShape.balanceOf(address[1])) == 0);
    });
});