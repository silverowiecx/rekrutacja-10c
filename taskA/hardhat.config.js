require("@nomiclabs/hardhat-ethers");

module.exports =  {
        solidity: {
          compilers: [
            {
              version: "0.5.16"
            },
            {
              version: "0.6.2"
            },
            {
              version: "0.6.4"
            },
            {
              version: "0.7.0"
            },
            {
              version: "0.8.0"
            },
            {
              version: "0.8.6"
            }
          ]
        },
        networks: {
          hardhat: {
          },
          fuji: {
            url: 'https://api.avax-test.network/ext/bc/C/rpc',
            gasPrice: 225000000000,
            chainId: 43113,
            accounts: ['']
          },
          mainnet: {
            url: 'https://api.avax.network/ext/bc/C/rpc',
            gasPrice: 225000000000,
            chainId: 43114,
            accounts: []
          }
        }
      }