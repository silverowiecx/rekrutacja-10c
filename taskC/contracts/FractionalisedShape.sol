import "@openzeppelin/contracts/token/ERC1155/ERC1155.sol";
import "@openzeppelin/contracts/token/ERC1155/extensions/ERC1155Burnable.sol";
import "@openzeppelin/contracts/token/ERC721/IERC721.sol";
import "@openzeppelin/contracts/token/ERC20/ERC20.sol";



pragma solidity ^0.8.0;


contract FractionalisedShape is ERC1155Burnable {

    address public tokenAddr;
    IERC721 private shape;
    IERC20 private shapeFund;

    
    mapping (uint256 => uint256) private _supplies;

    constructor(address _token, address _fund) public ERC1155(""){
        tokenAddr = _token;
        shape = IERC721(tokenAddr);
        shapeFund = IERC20(_fund);
    }

    function getSupply (uint256 id) external view virtual returns(uint256){
        return _supplies[id];
    }

    function mint(uint256 _id, uint256 _supply) public {
        require(_supplies[_id] == 0x0, "fractions for this token already exist");
        require(shape.ownerOf(_id) == msg.sender); // check if its their token (check if token exists is inside this method)
        require(shape.getApproved(_id) == address(this), "You need to set this contract as an operator beforehand"); // for ERC721 token transfer 
        require(shapeFund.balanceOf(msg.sender) > 0, "You need to own ShapeFund tokens in order to mint fractions!");
        _supplies[_id] = _supply;
        _mint(msg.sender, _id, _supply, "");
    }

    function redeem(uint256 _id) public {
        require(_supplies[_id] != 0, "Provided token (id) is not fractionalised");
        require(balanceOf(msg.sender, _id) == _supplies[_id], "You don't own all fractions");
        _burn(msg.sender, _id, _supplies[_id]);
        _supplies[_id] = 0;
        address _owner = shape.ownerOf(_id);
        shape.safeTransferFrom(_owner, msg.sender, _id);
    }
    

function safeTransferFrom(
        address from,
        address to,
        uint256 id,
        uint256 amount,
        bytes memory data
    )
        public
        virtual
        override
    {
        super.safeTransferFrom(from, to, id, amount, data);
        if(balanceOf(to, id) == _supplies[id]){
        address _owner = shape.ownerOf(id);
        shape.safeTransferFrom(_owner, to, id);
        }
    }

    function safeBatchTransferFrom(
        address from,
        address to,
        uint256[] memory ids,
        uint256[] memory amounts,
        bytes memory data
    )
        public
        virtual
        override
    {
        super.safeBatchTransferFrom(from, to, ids, amounts, data);
        for (uint256 i = 0; i < ids.length; ++i) {
            uint256 id = ids[i];
            if(balanceOf(to, id) == _supplies[id]){
                address _owner = shape.ownerOf(id);
                shape.safeTransferFrom(_owner, to, id);
            }
        }
    }

}