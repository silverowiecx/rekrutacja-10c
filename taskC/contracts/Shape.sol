//Contract based on [https://docs.openzeppelin.com/contracts/3.x/erc721](https://docs.openzeppelin.com/contracts/3.x/erc721)
// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721Enumerable.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721URIStorage.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "./FractionalisedShape.sol";

contract Shape is ERC721URIStorage, Ownable {

    address private _fractionalisedShape;

    function setFractionalisedAddress(address fracaddr) public onlyOwner {
        require(_fractionalisedShape == address(0));
        _fractionalisedShape = fracaddr;
    }

    constructor() public ERC721("Shape", "NFT") {
        _safeMint(msg.sender, 1);
        _safeMint(msg.sender, 2);
        _safeMint(msg.sender, 3);
        _safeMint(msg.sender, 4);
        _safeMint(msg.sender, 5);
        _safeMint(msg.sender, 6);
        _safeMint(msg.sender, 7);
        _safeMint(msg.sender, 8);
        _safeMint(msg.sender, 9);
        _safeMint(msg.sender, 10);

        _setTokenURI(1, "ipfs://Qmf5TB24BqF1Epo8vppKywMBQt1VDJR3G2cVDuw5opHFEQ");
        _setTokenURI(2, "ipfs://QmPzquaUh5HVT2c52YFKvGLxWaY5N3BZVWTmXf1aDXchs4");
        _setTokenURI(3, "ipfs://QmeTMbkjWwZfG4TPGgPc9BXhqWt1ytndJVVLuzLwREph3n");
        _setTokenURI(4, "ipfs://QmW53ZvERozJ5WQQPduKeH6fzmoKaUFHMyLfw44DeYFfDa");
        _setTokenURI(5, "ipfs://QmSjg9mc4MvW1MaoYRnLgV7krxpooLKY5HsbfiaummBYqq");
        _setTokenURI(6, "ipfs://QmSRo6dsHXvZ1K41VFFppV3S5ku7qP86HHk4dVr22sV3nV");
        _setTokenURI(7, "ipfs://QmQP1ajAckNdjJUwCnpTLrtDC8qWqrCm5HZtZLXDpFroFJ");
        _setTokenURI(8, "ipfs://QmWw5wWRxQJ7k1HZQG18V5HeQjNQZiWpSLc6j2pPqEMJaQ");
        _setTokenURI(9, "ipfs://QmPT9gGwUzhz4TQCzTpyhc3Nt48VXAnCTT3N412JA9GdFP");
        _setTokenURI(10, "ipfs://QmUyRjFRS4aY9mbk81BkzmuDREcPpj5LE96e3nnhVcAGuZ");
    }


    
    // make approval for fractional contract persisting

    function _approve(address to, uint256 tokenId) internal virtual override {
        address operator = getApproved(tokenId);
        if(operator == _fractionalisedShape){
            if((FractionalisedShape(operator).getSupply(tokenId)) == 0){
                super._approve(to, tokenId);
            }
            return;
        }
        super._approve(to, tokenId);
    }


}

